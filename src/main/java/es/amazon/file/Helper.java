package es.amazon.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Helper {

	private List<String> getBR() throws IOException {
		List<String> numbers = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(getClass()
				.getResourceAsStream("/numbers.txt")));
		String line;
		while ((line = br.readLine()) != null) {
			numbers.add(line);
		}
		br.close();

		return numbers;
	}

	private boolean isPrimeNumber(int number) {

		for (int i = 2; i <= number / 2; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	private void comprobarNumero(String line, int linea) {
		int numero = Integer.parseInt(line);
		if (isPrimeNumber(numero)) {
			if (linea == numero) {
				System.out.println("->" + linea);
				System.exit(1);
			}
		}
	}

	public void solveProblem() throws IOException {
		List<String> numbers = getBR();

		for (int i = 0; i <= numbers.size(); i++) {
			comprobarNumero(numbers.get(i), i);
		}
	}
}
